<?php

/**
* simplified example for md5, no salting
*/
function user_check_password($password, $account) {
  if ($account->pass == md5($password)) {
    return TRUE;
  }
  return FALSE;
}

/**
* Simplified example for md5 hashes, no salting
*/
function user_hash_password($password, $count_log2 = 0) {
  return md5($password);
}

/**
* this is probably a bad idea :-)
*/
function user_needs_new_hash($account) {
  return FALSE;
}